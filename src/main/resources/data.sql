--Franchise
INSERT INTO franchise (name, description) VALUES ('Marvel Studios','Marvels Comic Book Based Movies')
INSERT INTO franchise (name, description) VALUES ('Disney','All childhood favorites')
INSERT INTO franchise (name, description) VALUES ('Lord of the Rings','Stories from Tolkien')

--Movies
INSERT INTO movie (title,genre,year,director,image_url,trailer_url,franchise_id) VALUES ('The return of the king','fantasy,drama',2002,'Peter Jackson','image_url','trailer_url',3)
INSERT INTO movie (title,genre,year,director,image_url,trailer_url,franchise_id) VALUES ('Beauty and the beast','fantasy,drama',1999,'Minna Mankkala','image_url','trailer_url',2)
INSERT INTO movie (title,genre,year,director,image_url,trailer_url,franchise_id) VALUES ('Spiderman 2','fantasy, action,drama,comedy',2005,'Peter Jackson','image_url','trailer_url',1)

--Characters
INSERT INTO movie_char (full_name,alias,gender,photo_url) VALUES ('Peter Parker','Spiderman','male','photo_url')
INSERT INTO movie_char (full_name,alias,gender,photo_url) VALUES ('The Beast','Prince Adam','male','photo_url')
INSERT INTO movie_char (full_name,alias,gender,photo_url) VALUES ('Meriadoc Brandybuck','Merry','female','photo_url')
INSERT INTO movie_char (full_name,alias,gender,photo_url) VALUES ('Snow white','Lumikki','female','photo_url')

--Characters in movies
INSERT INTO movie_characters(movie_id,moviechar_id) VALUES(1,3)
INSERT INTO movie_characters(movie_id,moviechar_id) VALUES(2,2)
INSERT INTO movie_characters(movie_id,moviechar_id) VALUES(3,1)
INSERT INTO movie_characters(movie_id,moviechar_id) VALUES(2,4)