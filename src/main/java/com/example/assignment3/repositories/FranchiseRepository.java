package com.example.assignment3.repositories;

import com.example.assignment3.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    @Query("select f from Franchise f where f.name like %?1%")
    Collection<Franchise> findAllByName(String name);

}
