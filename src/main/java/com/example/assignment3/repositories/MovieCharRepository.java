package com.example.assignment3.repositories;

import com.example.assignment3.models.MovieChar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface MovieCharRepository extends JpaRepository<MovieChar, Integer> {
    @Query("select mc from MovieChar mc where mc.fullName like %?1%")
    Set<MovieChar> findAllByFullName(String fullName);

    //@Query("select mc from movie_char mc inner join movie_characters ON movie_characters.moviechar_id = mc.id where movie_characters.movie_id=?1")
    @Query(value = "select mc from movie_char mc inner join movie_characters on movie_characters.moviechar_id = mc.id where movie_characters.movie_id=?", nativeQuery = true)
    Set <MovieChar> findAllInMovie(Integer movieID);
}
