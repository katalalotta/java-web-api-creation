package com.example.assignment3.models.dtos;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.MovieChar;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
public class MovieGetDTO {
    private int id;
    private String title;
    private String genre;
    private int year;
    private String director;
    private String image_url;
    private String trailer_url;
    private Integer franchise;
    private Set<Integer> characters;
}
