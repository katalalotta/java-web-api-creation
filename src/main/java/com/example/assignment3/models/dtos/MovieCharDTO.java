package com.example.assignment3.models.dtos;

import com.example.assignment3.models.Movie;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
public class MovieCharDTO {
    private int id;
    private String fullName;
    private String alias;
    private String gender;
    private String photoUrl;
    private Set<Integer> movies;
}
