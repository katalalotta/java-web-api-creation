package com.example.assignment3.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class MovieChar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 255, nullable = false)
    private String fullName;
    @Column(length = 255)
    private String alias;
    @Column(length = 255)
    private String gender;
    @Column(length = 255)
    private String photoUrl;
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;

    @PreRemove
    private void removeMovieCharFromMovies() {
        for (Movie movie : movies) {
            movie.removeMovieChar(this);
        }
    }
}
