package com.example.assignment3.models;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Entity;

/**
 * Domain class (entity) to represent a movie.
 * Includes an auto generated key and some validation.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String title;
    @Column(length = 50, nullable = false)
    private String genre;
    @Column
    private int year;
    @Column(length = 50)
    private String director;
    @Column
    private String image_url;
    @Column
    private String trailer_url;
    @ManyToOne
    @JoinColumn(name="franchise_id")
    private Franchise franchise;
    @ManyToMany
    @JoinTable(
            name = "movie_characters",
            joinColumns = {@JoinColumn(name="movie_id")},
            inverseJoinColumns = {@JoinColumn(name="moviechar_id")}
    )
    private Set<MovieChar> characters;

    public void removeMovieChar(MovieChar movieChar) {
        for (MovieChar mChar : characters) {
            if (mChar.getId() == movieChar.getId()) {
                characters.remove(mChar);
            }
        }
    }
}
