package com.example.assignment3.services.character;

import com.example.assignment3.exceptions.MovieCharNotFoundException;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.repositories.MovieCharRepository;
import org.springframework.stereotype.Service;

import javax.persistence.PreRemove;
import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class MovieCharServiceImpl implements MovieCharService {
    private final MovieCharRepository movieCharRepository;

    public MovieCharServiceImpl(MovieCharRepository repo) { this.movieCharRepository = repo; }

    @Override
    public MovieChar findById(Integer id) {
        return movieCharRepository.findById(id).orElseThrow(() -> new MovieCharNotFoundException(id));
    }

    @Override
    public Collection<MovieChar> findAll() { return movieCharRepository.findAll(); }

    @Override
    public MovieChar add(MovieChar entity) { return movieCharRepository.save(entity); }

    @Override
    public MovieChar update(MovieChar entity) { return movieCharRepository.save(entity); }

    @Override
    @PreRemove
    public void deleteById(Integer id) { movieCharRepository.deleteById(id); }

    @Override
    public boolean exists(Integer id) { return movieCharRepository.existsById(id); }

    @Override
    public Collection<MovieChar> findAllByFullName(String name) {
        return movieCharRepository.findAllByFullName(name);
    }

    @Override
    public Collection<MovieChar> findAllInMovie(Integer movieID) {
        return movieCharRepository.findAllInMovie(movieID);
    }
}
