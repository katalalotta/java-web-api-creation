package com.example.assignment3.services.character;

import com.example.assignment3.models.MovieChar;
import com.example.assignment3.services.CrudService;

import java.util.Collection;

public interface MovieCharService extends CrudService<MovieChar, Integer> {
    Collection<MovieChar> findAllByFullName(String name);
    Collection<MovieChar> findAllInMovie(Integer movieID);
}
