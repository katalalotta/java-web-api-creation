package com.example.assignment3.services.movie;

import com.example.assignment3.models.Movie;
import com.example.assignment3.services.CrudService;

import java.util.Collection;
import java.util.Set;

public interface MovieService extends CrudService<Movie,Integer> {
    Collection<Movie> findAllByName(String name);

    Collection<Movie> findAllInFranchise(Integer franchiseID);

    void updateCharactersInMovie(int movieId, int [] IDs);
}
