package com.example.assignment3.services.movie;

import com.example.assignment3.models.Movie;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.repositories.MovieRepository;
import com.example.assignment3.exceptions.MovieNotFoundException;
import com.example.assignment3.services.character.MovieCharService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of the Movie service.
 * Uses the Movie repository to interact with the data store.
 * Logs errors through the standard logger.
 */
@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    private final MovieCharService movieCharService;

    public MovieServiceImpl(MovieRepository movieRepository, MovieCharService movieCharService) {
        this.movieRepository = movieRepository;
        this.movieCharService = movieCharService;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);

    }

    @Override
    public boolean exists(Integer id) {
        return movieRepository.existsById(id);
    }

    @Override
    public Collection<Movie> findAllByName(String name) {
        return movieRepository.findAllByName(name);
    }

    @Override
    public Collection<Movie> findAllInFranchise(Integer franchiseID) {
        return movieRepository.findAllInFranchise(franchiseID);
    }
    @Override
    public void updateCharactersInMovie(int movieId, int [] IDs) {
        Set<Integer> characterId = null;
        for (int i : IDs) {
            // Add each element into the set
            characterId.add(i);
        }

        Set<MovieChar> chrs = null;
        if (characterId != null) {
            chrs = characterId.stream()
                    .map(id -> movieCharService.findById(id)).collect(Collectors.toSet());
        }
        Movie movie = findById(movieId);
        movie.setCharacters(chrs);
        update(movie);
    }
}
