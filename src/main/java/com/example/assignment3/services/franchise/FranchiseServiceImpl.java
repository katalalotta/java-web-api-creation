package com.example.assignment3.services.franchise;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final FranchiseRepository franchiseRepository;
    public FranchiseServiceImpl(FranchiseRepository repo) { this.franchiseRepository = repo; }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() { return franchiseRepository.findAll(); }
    @Override
    public Franchise add(Franchise entity) { return franchiseRepository.save(entity); }
    @Override
    public Franchise update(Franchise entity) { return franchiseRepository.save(entity); }
    @Override
    public void deleteById(Integer integer) { franchiseRepository.deleteById(integer);}
    @Override
    public boolean exists(Integer integer) { return franchiseRepository.existsById(integer); }

    @Override
    public Collection<Franchise> findAllByName(String name){
        return franchiseRepository.findAllByName(name);
    }
}
