package com.example.assignment3.services.franchise;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.services.CrudService;

import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Collection<Franchise> findAllByName(String name);
    // INTERFACE!!!!!! AAAAAAA
}
