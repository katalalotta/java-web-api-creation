package com.example.assignment3.controllers;

import com.example.assignment3.exceptions.MovieNotFoundException;
import com.example.assignment3.mappers.MovieMapper;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.dtos.MovieGetDTO;
import com.example.assignment3.services.movie.MovieService;
import com.example.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Get movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("{id}") //GET: /api/v1/movies/1
    public ResponseEntity getById(@PathVariable int id) {
        MovieGetDTO movie = movieMapper.movieToMovieDTO(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }
    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping // GET: localhost:8080/api/v1/movies
    public ResponseEntity getAll() {
        Collection<MovieGetDTO> movies = movieMapper.movieToMovieDTO(
                movieService.findAll()
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary="Get movie by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No movie found with supplied name",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("search")
    public ResponseEntity findByName(@RequestParam String title){
        Collection<MovieGetDTO> movies = movieMapper.movieToMovieDTO(
                movieService.findAllByName(title)
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary="Get movies by franchise (franchise_id)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("infranchise")
    public ResponseEntity findByFranchise(@RequestParam Integer franchiseID){
        Collection<MovieGetDTO> movies = movieMapper.movieToMovieDTO(
                movieService.findAllInFranchise(franchiseID)
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary="Update characters in movie")
    @PutMapping("{id}/characters")
    public ResponseEntity updateCharacters(@RequestParam int id, @RequestParam int [] IDs){
        movieService.updateCharactersInMovie(id,IDs);
        return ResponseEntity.noContent().build();
    }


    @Operation(summary = "Add new movie, ID is autogenerated")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Movie successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PostMapping
    public  ResponseEntity add(@RequestBody Movie movie) {
        Movie m = movieService.add(movie);
        URI location = URI.create("movies/" + m.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary="Update by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody Movie movie, @PathVariable int id) {
        if (id != movie.getId())
            return ResponseEntity.badRequest().build();
        movieService.update(movie);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary="Delete by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
