package com.example.assignment3.controllers;

import com.example.assignment3.mappers.MovieCharMapper;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.models.dtos.MovieCharDTO;
import com.example.assignment3.models.dtos.MovieGetDTO;
import com.example.assignment3.services.character.MovieCharService;
import com.example.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class MovieCharController {
    private final MovieCharService movieCharService;

    private final MovieCharMapper movieCharMapper;

    public MovieCharController(MovieCharService serv, MovieCharMapper mcMapper) {
        this.movieCharService = serv;
        this.movieCharMapper = mcMapper;
    }

    @Operation(summary = "Get character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("{id}") //GET: /api/v1/characters/1
    public ResponseEntity getById(@PathVariable int id) {
        MovieCharDTO ch = movieCharMapper.movieCharToMovieCharDTO(
                movieCharService.findById(id)
        );
        return ResponseEntity.ok(ch);
    }
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping // GET: localhost:8080/api/v1/characters
    public ResponseEntity getAll() {
        Collection<MovieCharDTO> chs = movieCharMapper.movieCharToMovieCharDTO(
                movieCharService.findAll()
        );
        return ResponseEntity.ok(chs);
    }

    @Operation(summary="Get character by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No Character found with supplied name",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("search")
    public ResponseEntity findByName(@RequestParam String name){
        Collection<MovieCharDTO> chs = movieCharMapper.movieCharToMovieCharDTO(
                movieCharService.findAllByFullName(name)
        );
        return ResponseEntity.ok(chs);
    }

    @Operation(summary="Get characters by movie (movie_id)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    }
    )
    @GetMapping("inmovie")
    public ResponseEntity findByMovie(@RequestParam Integer movieID){
        Collection<MovieCharDTO> characters = movieCharMapper.movieCharToMovieCharDTO(
                movieCharService.findAllInMovie(movieID)
        );
        return ResponseEntity.ok(characters);
    }


    @Operation(summary = "Add new character, ID is autogenerated")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Character successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PostMapping
    public  ResponseEntity add(@RequestBody MovieChar mc) {
        MovieChar m = movieCharService.add(mc);
        URI location = URI.create("characters/" + m.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary="Update by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody MovieChar mc, @PathVariable int id) {
        if (id != mc.getId())
            return ResponseEntity.badRequest().build();
        movieCharService.update(mc);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary="Delete by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieCharService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
