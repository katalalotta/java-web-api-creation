package com.example.assignment3.exceptions;

public class MovieCharNotFoundException extends RuntimeException {
    public MovieCharNotFoundException(int id) { super("Movie character with ID " + id + "does not exist."); }

    public MovieCharNotFoundException() { super(); }
    public MovieCharNotFoundException(String message) { super(message); }
    public MovieCharNotFoundException(String message, Throwable cause) { super(message, cause); }
    public MovieCharNotFoundException(Throwable cause) { super(cause); }
    protected MovieCharNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
