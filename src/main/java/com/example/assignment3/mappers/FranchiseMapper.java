package com.example.assignment3.mappers;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.models.dtos.FranchiseDTO;
import com.example.assignment3.models.dtos.MovieGetDTO;
import com.example.assignment3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel="spring")
public abstract class FranchiseMapper {

    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIDs")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises);


    @Mapping(target="movies", source="movies",qualifiedByName = "movieIDsToMovies")
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO dto);

    @Named("movieIDsToMovies")
    Set<Movie> mapIDsToMovies(Set<Integer> id){
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviesToIDs")
    Set<Integer> mapMoviesToIDs(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


}
