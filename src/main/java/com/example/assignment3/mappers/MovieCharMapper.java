package com.example.assignment3.mappers;

import com.example.assignment3.models.Movie;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.models.dtos.MovieCharDTO;
import com.example.assignment3.models.dtos.MovieGetDTO;
import com.example.assignment3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieCharMapper {

    @Autowired
    protected MovieService movieService;

    @Mapping(target="movies",source="movies",qualifiedByName="moviesToIDs")
    public abstract MovieCharDTO movieCharToMovieCharDTO(MovieChar mc);

    public abstract Collection<MovieCharDTO> movieCharToMovieCharDTO(Collection<MovieChar> mcharacters);
    @Mapping(target="movies",source="movies",qualifiedByName="movieIDsToMovies")
    public abstract MovieChar movieCharDTOToMovieChar(MovieCharDTO dto);

    @Named("movieIDsToMovies")
    Set<Movie> mapIDsToMovies(Set<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviesToIDs")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

}
