package com.example.assignment3.mappers;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.MovieChar;
import com.example.assignment3.models.dtos.MovieGetDTO;
import com.example.assignment3.services.character.MovieCharService;
import com.example.assignment3.services.character.MovieCharServiceImpl;
import com.example.assignment3.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected FranchiseService franchiseService;

    @Autowired
    protected MovieCharService movieCharService;

    @Mapping(target="franchise", source="franchise.id")
    @Mapping(target="characters",source="characters", qualifiedByName = "charactersToIDs")
    public abstract MovieGetDTO movieToMovieDTO(Movie movie);

    public abstract Collection<MovieGetDTO> movieToMovieDTO(Collection<Movie> movies);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIDToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIDsToCharacters")
    public abstract Movie movieDTOToToMovie(MovieGetDTO dto);

    @Named("franchiseIDToFranchise")
    Franchise mapIDToFranchise(int id) {
        return franchiseService.findById(id);
    }
    @Named("characterIDsToCharacters")
    Set<MovieChar> mapIDsToCharacters(Set<Integer> id) {
        return id.stream()
                .map( i -> movieCharService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("charactersToIDs")
    Set<Integer> mapSubjectsToIds(Set<MovieChar> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }



}
