# Java Assignment 3

The Java Assignment 3 is a program in which it is possible to handle different types of entities related to movies. These entities are **Movie Characters**, **Movies**, and **Franchises**. Data of these entities is stored in a **PostgreSQL**-database. It is possible to alter and check the data through an **API**.

The application is being deployed by first building it using **GitLab CI/CD**, and the pushing it to an existing **Heroku project.**

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

**For development:**

 * IntelliJ (preferably Ultimate Edition) with Java 17.
 * PostgreSQL with PgAdmin.
 * Docker.

**For usage:**

 * A web-browser.

## Usage

**For development:**

Clone the repository, and build the application

**For end-users:**

Head to, for an example [this address](https://java-api-creation-test-app.herokuapp.com/api/v1/characters) to check all the movie characters out.

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

[Lotta Lampola @katalalotta](https://gitlab.com/katalalotta)

## Contributing

List of contributors

## License

MIT
